<div dir=rtl>

# wiki-trans-wishlist
لیست صفحات ویکی که کاربران درخواست ترجمه‌ی آن‌ها را دارند به‌همراه وضعیت ترجمه

</div>
 
 <div dir=rtl>
 
# ترجمه نشده
</div>

- [ ] [Vandalism](https://wiki.openstreetmap.org/wiki/Vandalism)
- [ ] [Getting Involved](https://wiki.openstreetmap.org/wiki/Getting_Involved)
- [ ] [Map Features](https://wiki.openstreetmap.org/wiki/Map_Features)

<div dir=rtl>

# ترجمه ناقص
[لیست همهٔ ترجمه‌های ناقص در ویکی](https://wiki.openstreetmap.org/wiki/Category:Fa:Translation_not_complete) - همهٔ آن‌ها جزو موارد درخواستی نیست!
</div>
 
- [ ] [برخی استانداردها و قواعد ویرایش نقشه](https://wiki.openstreetmap.org/wiki/Fa:Editing_Standards_and_Conventions) 


<div dir=rtl>

# ترجمه شده
</div>

- [x] [Any tags you like](https://wiki.openstreetmap.org/wiki/Fa:Any_tags_you_like)
